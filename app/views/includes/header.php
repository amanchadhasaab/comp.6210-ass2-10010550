<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accounting</title>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>assets/css/Footer-Clean.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>assets/css/styles.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>assets/css/Testimonials.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>assets/css/styles.css">
</head>

<body>
<nav class="navbar navbar-light navbar-expand-md">
        <div class="container-fluid"><a class="navbar-brand" href="#" style="background-image:url(&quot;/assets/images/GeyserlandSBA-flattened.png&quot;);width:190px;height:120px;background-color:#eaeaea;background-repeat:no-repeat;background-size:120%;background-position:top;color:rgba(7,7,7,0.9);"></a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navcol-1">
                    <ul class="nav navbar-nav ml-auto" style="background-color:#bbb8b8;">
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="/Pages/Home" target="_blank" style="font-size:16px;"><strong>Home</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/Pages/Contact"><strong>Contact</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/Pages/About"><strong>About</strong></a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/Pages/Services"><strong>Services</strong></a></li>
                        <l1><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#SignUp">
                                Sign Up
                            </button></li>
                    </ul>
                </div>
        </div>
    </nav>
    <div class="modal fade" id="SignUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/">
                    <div class="modal-body">
                        <div class="container">
                            <h1>Sign Up</h1>
                            <p>Please fill in this form to create an account.</p>
                            <hr>
                            <label for="email">
                                <b>Email</b>
                            </label>
                            <br>
                            <input type="text" placeholder="Enter Email" name="email" required>
                            <br>
    
                            <label for="psw">
                                <b>Password</b>
                            </label>
                            <br>
                            <input type="password" placeholder="Enter Password" name="psw" required>
                            <br>
    
                            <label for="psw-repeat">
                                <b>Repeat Password</b>
                            </label>
                            <br>
                            <input type="password" placeholder="Repeat Password" name="psw-repeat" required>
    
                            <label>
                                <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
                            </label>
    
                            <p>By creating an account you agree to our
                                <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
</form>
                </div>
            </div>
    </div>